const cvs = document.getElementById("canvas");
const ctx = cvs.getContext("2d");
ctx.canvas.width  = window.innerWidth;
ctx.canvas.height = window.innerHeight;

//=======================================================================================
  // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyD5-G2qSPCGPSFUdXTZohfrvPpbS6OF6RQ",
        authDomain: "flappybirdgame-5ec0c.firebaseapp.com",
        databaseURL: "https://flappybirdgame-5ec0c.firebaseio.com",
        projectId: "flappybirdgame-5ec0c",
        storageBucket: "flappybirdgame-5ec0c.appspot.com",
        messagingSenderId: "234718792563",
        appId: "1:234718792563:web:efb05778f343b991da1306"
    };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var database = firebase.database();
  var ref = database.ref('scores');
  ref.orderByChild('scores').on('value', gotData, err);

  function gotData(data){
        scores = data.val();
        let values = Object.values(scores);
        for(let i=0; i<values.length; i++){
            if(values[i] > bestScore){
                bestScore = values[i];
            }
        }
  }
  function err(e){
    console.log(e);
}
//=======================================================================================


const gap = 100;
let birdX = 10;
let birdY = 150;
let gravity = 1;
let score = 0;
let scores = [];
let bestScore = 0;

const bird = new Image();
const background = new Image();
const ground = new Image();
const upPipe = new Image();
const downPipe = new Image();
const blackHole = new Image();

bird.src = "img/bird.png";
background.src = "img/background.png";
ground.src = "img/ground.png";
upPipe.src = "img/upPipe.png";
downPipe.src = "img/downPipe.png";
blackHole.src = "img/blackHole.png";

let pipe  = [{x:cvs.width, y:0}];
	
window.onload = function(){
		ctx.drawImage(background, 0, 0, cvs.width, cvs.height);

        for(let i=0; i<pipe.length; i++){

            ctx.drawImage(upPipe, pipe[i].x, pipe[i].y);

            ctx.drawImage(downPipe, pipe[i].x, pipe[i].y + upPipe.height + gap, downPipe.width, cvs.height - (pipe[i].y + upPipe.height + gap));
            pipe[i].x -= 1; 
    
            if(pipe[i].x == Math.round(cvs.width*0.3)){
                pipe.push({
                    x : cvs.width,
                    y : Math.random() * upPipe.height - upPipe.height
                });
            }
				if(
				(birdX + bird.width == pipe[i].x && ( birdY <= pipe[i].y + upPipe.height || birdY + bird.height >= pipe[i].y + upPipe.height + gap))
				|| (birdX + bird.width  >= pipe[i].x && birdX <= pipe[i].x + upPipe.width && (birdY == Math.floor(pipe[i].y + upPipe.height) || birdY + bird.height == Math.floor(pipe[i].y + upPipe.height + gap)))
				|| (birdY + bird.height == cvs.height - ground.height)
				|| (Math.floor(birdY) <= 0)){
                    ref.push(score);
					window.navigator.vibrate(300);
					this.location.reload();

            }
    
            if(pipe[i].x + upPipe.width == Math.round(birdX) ){
                score++;
            }
    
            if(score % 3 == 0 && score > 1){
                let blackHoleX = cvs.width*0.8;
                let blackHoleY = (cvs.height-ground.height)*0.1;
                let blackHoleHeight = cvs.height * 0.07;
                let blackHoleWidth = cvs.width * 0.14;
    
                ctx.drawImage(blackHole, blackHoleX, blackHoleY, blackHoleWidth, blackHoleHeight); 
                    
                gravity = -1;
            }
            else{
                gravity = 1;
            }
        }
    
    
        ctx.drawImage(ground, 0, cvs.height - ground.height, cvs.width, ground.height);
        ctx.drawImage(bird, birdX, birdY);
    
        birdY += gravity;
    
        ctx.fillStyle = "#000";
        ctx.font = cvs.height*0.08 + "px Verdana";
        ctx.fillText(score, cvs.width*0.45, cvs.height*0.1);
        ctx.font = cvs.height*0.03 + "px Verdana";
        ctx.fillText("World Record: " + bestScore,cvs.width*0.1, cvs.height*0.9);

        requestAnimationFrame(window.onload);
    }

document.addEventListener("touchstart", moveUp);

function moveUp(){
    if(score % 3 == 0 && score > 1){
        birdY -= -25;
    }
    else{
        birdY -= 25;
    }
}
